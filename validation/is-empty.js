module.exports = function isEmpty(value) {
  return(
    value === undefined ||
    value === null ||
    (typeof value === 'object' && Object.keys(value).length === 0) ||
    (typeof value === 'string' && value.trimRight().length === 0)
  );
};

    